# If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
# Find the sum of all the multiples of 3 or 5 below 1000.

i3 = 1
i5 = 1
s  = 0

while ( i3*3 < 1000 or i5*5 < 1000):
    if (i3*3 < i5*5):
        s += i3 * 3
        i3 = i3 + 1
    elif (i3*3 == i5*5):
        s += i3 * 3
        i3 = i3 + 1
        i5 = i5 + 1
    else:
        s += i5 * 5
        i5 = i5 + 1
print s
        
